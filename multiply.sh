#!/bin/sh

set -e
#set -x

binary_fname="min"
config_fname="config.ini"
app_start_port=12000
cli_start_port=14000
json_rpc_start_port=16000
bootstrap_host="u-pl1.ms.mff.cuni.cz"
bootstrap_port=9000

get_ip() {
	ifconfig eth0 | grep inet | grep -v inet6 | sed 's,.*inet ,,;s, .*,,'
}

sha() {
	echo -n "$1" | shasum | cut -d' ' -f1
}

get_instance_hash() {
	if [ $# -ne 2 ]; then
		echo "get_instance_hash ip node_number"
		exit 1
	fi

	sha "$1/$2"
}

generate_config() {
	if [ $# -ne 2 ]; then
        	echo "generate_config ip node_number"
        	exit 1
	fi

	cat <<END
[overlay]
local_id=$(get_instance_hash "$1" "$2")
[server]
public_addr=$1
port=$(expr "$app_start_port" + "$2")
[cli]
addr=0.0.0.0
port=$(expr "$cli_start_port" + "$2")
[json_rpc]
port=$(expr "$json_rpc_start_port" + "$2")
[bootstrap_nodes]
$bootstrap_host=$bootstrap_port
END
}

generate_instance() {
	if [ $# -ne 3 ]; then
		echo "generate_instance app_binary ip node_number"
		exit 1
	fi

	path="nodes/$3"
	
	mkdir -p "$path"

	ln -s "$1" "$path/$binary_fname"

	generate_config "$2" "$3" > "$path/$config_fname"

	#mkfifo "$path/input"
	#mkfifo "$path/output"
}

generate() {
	if [ $# -ne 2 ]; then
		echo "generate app_binary node_count"
		exit 1
	fi

	app_binary="$1"
	node_count="$2"

	if [ ! -x $app_binary ]; then
		echo "Invalid path to binary"
		exit 1
	fi

	ip=$(get_ip)

	for i in $(seq 1 "$node_count"); do
		echo "Generating node number $i"
		generate_instance "$1" "$ip" "$i"
	done
}

run_instance() {
	if [ $# -ne 1 ]; then
		echo "run_instance node_number"
		exit 1
	fi

	#nohup sh -c "
	(
		cd "nodes/$1"

		if [ -e PID ]; then
			echo "node $1 already running"
			exit 1
		fi

		./"$binary_fname" 2>/dev/null >/dev/null &
		echo $! > PID
		#wait
		#echo "node $1 stopped"
		#rm PID
	#)" &
	) &
	#disown

	sleep 1
}

run() {
	if [ $# -ne 1 ]; then
		echo "run node_count"
		exit 1
	fi

	for i in $(seq 1 "$1"); do
		echo "Runnung node number $i"
		run_instance "$i"
	done
}

usage() {
	echo "Usage:"
	echo "	$0 generate path/to/binary node_count"
	echo "		to create node_count instances - symlink binary to multiple folders and create config files"
	echo "	or"
	echo "	$0 run node_count"
	echo "		to run first node_count nodes"
}

if [ $# -lt 1 ]; then
	usage
	exit 1
fi

case "$1" in
generate)
	if [ $# -ne 3 ]; then
		usage
		exit 1
	fi
	generate "$2" "$3";;
	
run)
	if [ $# -ne 2 ]; then
		usage
		exit 1
	fi
	run $2;;
	
*)
	usage;
	exit 1;;
esac
